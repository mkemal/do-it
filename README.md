# Do-it!
JavaScript Grundkurs Projekt für die Weiterbildungsmaßnahme bei Alfatraining.

14.02.2022 bis 18.02.2022

## Authoren
+ Mustafa Kemal Öcel [GitHub](https://github.com/mkemaloecel)
+ Agron Kadriaj [GitHub](https://github.com/KKA11010) [GitLab](https://gitlab.com/FirstTerraner)

## Geschrieben mit Hilfe von:
+ JavaScript Nativ
+ PHP (XAMPP Server)
+ AJAX (JSON)
+ HTML5/CSS

## Dieses Projekt bietet folgende Features:
+ Mehrere Benutzer und individuelle To-Do Listen in einer JSON-Datei Erstellen
+ Login/Logout (Simuliert mit Local Storage)
+ Benutzername validierung (Duplikate)
+ Passwort Stärke (Achtung: Passwörter werden in plain-text gespeichert)
+ To-Do Liste Erfassen
+ Einträge erstellen
+ Einträge bearbeiten
+ Einträge sortieren
+ Einträge löschen
+ Mehrere erledigte Einträge löschen

## Installation
+ XAMPP wird benötigt: [XAMPP](https://www.apachefriends.org/de/download.html)
+ Erstelle einen Ordner mit dem Namem "server" unter "C:\xampp\htdocs\"
+ Git Projekt´klonen unter "C:\xampp\htdocs\server\"
+ XAMPP Server als Admin starten
+ Navigiere zu [localhost](http://localhost/server/do-it/index.html)

## Programm klonen
```bash
git clone https://gitlab.com/mkemal/do-it.git
# change directory
cd do-it
```

## Fehlermeldungen
Öffne ein "Issue" um den Fehler öffentlich zu besprechen oder implementiere eine Lösung und erstelle ein Pull-request.
