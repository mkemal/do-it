import { getElBySelector, createXHR, setPTagMain, createNewJson } from './dom_helper.js'

"use strict"

init()

function init() {
	const loginForm = getElBySelector('#loginform')
	const nameInput = getElBySelector('#username')
	const pwInput = getElBySelector('#password')

	loginForm.onsubmit = function () {
		let username = nameInput.value
		let password = pwInput.value
		submitLogin(username, password)
		// Absenden des Formulars unterbinden
		return false
	}
}

function submitLogin(username, pw) {
	let xhr = createXHR()
	xhr.onload = function () {
		if (xhr.status !== 200) {
			console.log("file not found")
			createNewJson()
			return
		}
		const usersResponse = JSON.parse(xhr.responseText)
		const user = getAndCheckUser(username, pw, usersResponse.users)
		// bad case
		if (!user) {
			badLoginNotify()
			return
		}
		// good case, write in localStorage
		localStorage.setItem('user', user.username)
		location.href = './todoList.html'
	}
	xhr.open('GET', 'users.json')
	xhr.send()
}

function getAndCheckUser(username, pw, users) {
	let userMatch
	// check user and pw
	for (let i = 0; i < users.length; i++) {
		const user = users[i]
		// good case, set userMatch var
		if (username === user.username && pw === user.password) {
			userMatch = user
			break
		}
	}
	return userMatch
}

function badLoginNotify() {
	const container = getElBySelector('#main-content')
	const paragraf = getElBySelector('#badNotify')
	let color = 'red'
	let innerText = 'Benutzername oder Passwort falsch!'
	setPTagMain(paragraf, innerText, color, container)
}