import {
	getElBySelector,
	createEle,
	createXHR,
	sendFile,
	formatDateString,
	logoutUser,
	createNewJson,
	addModalListeners,
	formatMinutesToHrsMin,
	getEstimatedFromNode,
	updateButtonState,
	getEstValues,
	getUserFromJson
} from './dom_helper.js'

import { addNewToDo, sortOnChange, deleteCompleted, addItem } from './events.js'

"use strict"

// main elements
let container = getElBySelector('#main-content')
let tbody = getElBySelector("tbody")
// users & todos data
let json

// init
init()

function init() {
	// Elemente selektieren
	let logoutBtn = getElBySelector('#logout')
	let loginBtn = getElBySelector('#login')
	let addButton = getElBySelector("button#addItem")
	let sortSelect = getElBySelector('#sortEntries')
	let deleteCompletedEntries = getElBySelector('#deleteCompletedEntries')
	let todoModal = getElBySelector('#todoModalId')
	let openModalBtn = getElBySelector('#openAddModal')
	let loggedUser = localStorage.getItem('user')
	let inputDueDate = getElBySelector('#dueDate')
	let userPlaceholder = getElBySelector('#username')
	// check if user is "logged in"
	if (loggedUser) {
		userPlaceholder.innerText = loggedUser
		if (tbody) { tbody.innerHTML = "" }
		fillToDoList()
		logoutBtn.hidden = false
	} else {
		userPlaceholder.innerText = ''
		container.innerHTML = '<h2>Melde dich an um Do-it! zu nutzen...</h2>'
		if (loginBtn) { loginBtn.hidden = false }
	}
	// set default deadline input to today
	inputDueDate.value = new Date().toISOString().split('T')[0]
	// eventListener für Buttons erzeugen
	// logout button
	logoutBtn.onclick = logoutUser
	// login button
	loginBtn.onclick = function () { location.href = './login.html' }
	// add new TO-DO
	addButton.onclick = addNewToDo
	// delete all completed todos
	deleteCompletedEntries.onclick = deleteCompleted
	// sort TO-DOs
	sortSelect.onchange = function (e) { sortOnChange(e) }
	// todo modal events
	if (openModalBtn) {
		openModalBtn.onclick = function () {
			todoModal.style.display = 'block'
		}
	}
	addModalListeners(todoModal)
	addModalListeners(getElBySelector('#updateModalId'))
	appendActionRow()
	updateButtonState()
}

// Exportierte Funktionen
export function deleteRow(node) {
	// Zeile löschen, in der Button sich befindet
	node.remove()
	updateButtonState()
	saveUpdate()
}

export function saveUpdate() {
	try {
		let allTableRows = getElBySelector('tbody').querySelectorAll("tr.row")
		const user = getUserFromJson(localStorage.getItem('user'), json.users)
		if (user) {
			user.todoList = []
			for (let i = 0; i < allTableRows.length; i++) {
				const oneRow = allTableRows[i]
				let cellList = oneRow.querySelectorAll('td')
				if (cellList) {
					let title = cellList[0].innerText
					let createdAt = formatDateString(cellList[1].innerText)
					let dueDate = formatDateString(cellList[2].innerText)
					let estTime = getEstimatedFromNode(cellList[3])
					let done = cellList[4].firstChild.checked
					let newTodo = {
						title: title,
						createdAt: createdAt,
						dueDate: dueDate,
						estTime: estTime,
						done: done
					}
					user.todoList.push(newTodo)
				}
			}
			sendFile(JSON.stringify(json), "./php/script.php")
		}
	} catch (error) {
		console.log("Speichern hat ein Fehler: ", error)
	}
}

export function createDeleteButton() {
	let button = createEle("button", 'x')
	button.classList.add("removeItem")
	button.addEventListener("click", function (e) {
		e.stopPropagation()
		deleteRow(this.parentNode.parentNode)
	})
	return button
}

export function createButtonUp() {
	let button = createEle("button", "↑")
	button.className = "nachOben"
	button.addEventListener("click", function (e) {
		e.stopPropagation()
		let currentRow = this.parentNode.parentNode
		let prevRow = currentRow.previousElementSibling
		prevRow.before(currentRow)
		updateButtonState()
		saveUpdate()
	})
	return button
}

export function createButtonDown() {
	let button = createEle("button", "↓")
	button.className = "nachUnten"
	button.addEventListener("click", function (e) {
		e.stopPropagation()
		let currentRow = this.parentNode.parentNode
		let nextRow = currentRow.nextElementSibling
		nextRow.after(currentRow)
		updateButtonState()
		saveUpdate()
	})
	return button
}

// ausgelagerte locale Funktionen
function appendActionRow() {
	// Spalte im thead erzeugen
	let actionsTh = createEle("th", "Aktionen")
	let tr = getElBySelector("table#liste thead tr")
	if (tr) {
		tr.appendChild(actionsTh)
		let allTableRows = tbody.querySelectorAll("tr")
		for (let i = 0; i < allTableRows.length; i++) {
			allTableRows[i].appendChild(createEle("td"))
		}
		// letzte Zellen in Zeilen selektieren
		let lastCells = document.querySelectorAll("table#liste td:last-child")
		// Action-Button in letzte Zellen integrieen
		for (let i = 0; i < lastCells.length; i++) {
			lastCells[i].appendChild(createDeleteButton())
			lastCells[i].appendChild(createButtonUp())
			lastCells[i].appendChild(createButtonDown())
		}
	}
}

function fillToDoList() {
	let xhr = createXHR()
	xhr.onload = function () {
		if (xhr.status !== 200) {
			console.log("file not found")
			createNewJson()
			return
		}
		json = xhr.response
		addTableItems()
	}
	xhr.open("GET", "./users.json")
	xhr.responseType = "json"
	xhr.setRequestHeader("Cache-Control", "no-cache")
	xhr.send()
}

function addTableItems() {
	const user = getUserFromJson(localStorage.getItem('user'), json.users)
	if (user) {
		let todoList = user.todoList
		if (todoList.length < 1) {
			getElBySelector('#liste').innerHTML = ''
			getElBySelector('#tableControls').style.display = 'none'
			getElBySelector('#emptyToDo').style.display = 'block'
			getElBySelector('#addFirstToDo').onclick = function () {
				getElBySelector('#todoModalId').style.display = 'block'
			}
			return
		}
		for (let i = 0; i < todoList.length; i++) {
			let todo = todoList[i].title
			let done = todoList[i].done
			let createdAt = todoList[i].createdAt
			let dueDate = todoList[i].dueDate
			let estVals = getEstValues(formatMinutesToHrsMin(todoList[i].estTime))
			// let estTime = formatMinutesToHrsMin(todoList[i].estTime)
			addItem(todo, done, dueDate, estVals.std, estVals.min, createdAt)
		}
	}
}