import {
	createEle,
	createXHR,
	getElBySelector,
	getUserFromJson,
	logoutUser,
	createNewJson
} from "./dom_helper.js"

"use strict"

init()

function init() {
	let logoutBtn = getElBySelector('#logout')
	// see if user in "logged-in"
	let userLocalStorage = localStorage.getItem('user')
	// show logout button if user is in local storage
	if (userLocalStorage) {
		logoutBtn.hidden = false
		logoutBtn.onclick = logoutUser
	}
	// get data
	let usersInDB
	let xhr = createXHR()
	xhr.onload = function () {
		if (xhr.status != 200) {
			console.log("file not found")
			createNewJson()
			return
		}
		usersInDB = JSON.parse(xhr.responseText)
		const user = getUserFromJson(userLocalStorage, usersInDB.users)
		if (!user) {
			getElBySelector('#main-content-h1').innerText = 'Melde dich an um "Do-it!" zu nutzen...'
			return
		}
		// show user data
		renderLoggedIn(user)
	}
	xhr.open('GET', 'users.json')
	xhr.send()
}

function renderLoggedIn(user) {
	// show user message and button to redirect to the todo page
	let container = getElBySelector('#main-content')
	container.innerHTML = ''
	const example = createEle('h1', `Willkommen ${user.username}`)
	const button = createEle('button', 'To-Do\'s ansehen')
	button.classList.add('myBtn', 'greenBtn')
	button.onclick = function () {
		location.href = './todoList.html'
	}
	container.append(example, button)
}
