import {
	createEle,
	setPTag,
	getEstimatedFromNode,
	checkExistingTodo,
	isValidDeadline,
	getElBySelector,
	formatEstimatedTime,
	updateButtonState,
	getDuedFromNode,
	formatDateString,
	getEstValues,
	appendTable,
	isValidEstimated
} from './dom_helper.js'

import {
	createDeleteButton,
	createButtonUp,
	createButtonDown,
	saveUpdate,
	deleteRow,
} from './todo_list.js'

"use-strict"

// to-do event-handlers
export function addNewToDo() {
	let p = getElBySelector('#todoMessage')
	// user inputs
	let inputName = getElBySelector("input.title")
	let inputDueDate = getElBySelector("input.dueDate")
	let inputEstTime = getElBySelector("input.estTime")
	let inputEstMin = getElBySelector('#estimatedMinutes')
	let createdAt = new Date()
	// validation
	let color = "red"
	let textContent
	if (checkExistingTodo(inputName.value)) {
		textContent = "\"" + inputName.value + "\" existiert bereits!"
		setPTag(p, textContent, color)
		return
	}
	if (!isValidDeadline(new Date(inputDueDate.value), createdAt)) {
		setPTag(p, "Invalide Deadline!", color)
		return
	}
	if (!isValidEstimated(inputEstTime.value, inputEstMin.value)) {
		setPTag(p, "Invalide Zeit-Schätzung!", color)
		return
	}
	// add new todo
	addItem(
		inputName.value, // to-do Title
		false, // Erledigt
		new Date(inputDueDate.value), // Deadline
		+inputEstTime.value, // Zeit-Schätzung (std)
		+inputEstMin.value, // Zeit-Schätzung (min)
		createdAt // Erstelldatum
	)
}

export function sortOnChange(e) {
	let tbody = getElBySelector("tbody")
	let todoList = tbody.querySelectorAll('tr.row')
	// get select option value
	let option = e.target.value
	if (option == '') { return }
	// convert nodelist to array
	let nodeArray = Array.from(todoList)
	if (option == 'estimated') {
		// sort by estimated time
		nodeArray.sort(function (a, b) {
			// Schwerer Part: Konvertierung der Angezeigten Zeit in Minuten um leichter zu sortieren
			let valueA = getEstimatedFromNode(a.querySelector('.estimated'))
			let valueB = getEstimatedFromNode(b.querySelector('.estimated'))
			return valueA > valueB ? 1 : -1
		})
	} else {
		//sort by deadline
		nodeArray.sort(function (a, b) {
			let valueA = getDuedFromNode(a.querySelector('.dueDate'))
			let valueB = getDuedFromNode(b.querySelector('.dueDate'))
			return valueA > valueB ? 1 : -1
		})
	}
	tbody.innerHTML = ''
	// append each node into document
	nodeArray.forEach(todo => tbody.append(todo))
	updateButtonState()
	saveUpdate()
}

export function deleteCompleted() {
	let todoList = getElBySelector("tbody").querySelectorAll('tr.row')
	for (let i = 0; i < todoList.length; i++) {
		let checked = todoList[i].querySelector('.checkboxTd').firstChild.checked
		if (checked) {
			deleteRow(todoList[i])
		}
	}
}

export function addItem(todo, done, dueDate, estTime, estMin, createdAt) {
	let tbody = getElBySelector("tbody")
	// create todo row
	let tableRow = createToDoRow(todo, done, dueDate, estTime, estMin, createdAt)
	// update todo-entry event
	tableRow.onclick = function (e) {
		handleTableRowClick(e, this)
	}
	// adding first todo page has no table and no tbody, so
	// append new table after first todo
	if (!tbody) {
		appendTable(tableRow)
		getElBySelector('#tableControls').style.display = 'flex'
		getElBySelector('#emptyToDo').style.display = 'none'
		saveUpdate()
		return
	}
	tbody.append(tableRow)
	updateButtonState()
	saveUpdate()
}

function createToDoRow(todo, done, dueDate, estTime, estMin, createdAt) {
	// creates a todo row in HTML Elements
	// to-do name
	let itemTd = createEle("td", todo)
	itemTd.className = "todoId"
	// checkbox warp
	let doneTd = createEle('td')
	doneTd.className = 'checkboxTd'
	// checkbox
	let doneContent = createEle('input')
	doneContent.type = 'checkbox'
	doneContent.checked = done
	// checkbox event
	doneContent.onchange = function (e) {
		e.stopPropagation()
		doneContent.checked = (this.checked)
		saveUpdate()
	}
	doneTd.append(doneContent)
	// deadline
	let dueDateTd = createEle("td", new Date(dueDate).toLocaleDateString())
	dueDateTd.className = 'dueDate'
	// estimated time
	let estTimeTd = createEle("td", formatEstimatedTime(estTime, estMin))
	estTimeTd.className = 'estimated'
	// created at
	let createdAtTd = createEle("td", new Date(createdAt).toLocaleDateString())
	// create last action cell
	let actionsTd = createEle("td")
	actionsTd.className = "lastTH"
	actionsTd.append(createDeleteButton(todo), createButtonUp(), createButtonDown())
	// row
	let tableRow = createEle("tr")
	tableRow.className = "row"
	tableRow.append(itemTd, createdAtTd, dueDateTd, estTimeTd, doneTd, actionsTd)
	return tableRow
}

function handleTableRowClick(e, node) {
	// dont open the edit modal on checkbox click
	if (e.target?.attributes?.['type']?.nodeValue == 'checkbox') {
		return
	}
	// open update modal
	let updateModal = getElBySelector('#updateModalId')
	updateModal.style.display = 'block'

	// node current values
	let cellList = node.querySelectorAll('td')
	let title = cellList[0].innerText
	let dueDate = formatDateString(cellList[2].innerText)
	// let estTimeee = getEstimatedFromNode(cellList[3]) // minutes fo DB
	let estVals = getEstValues(cellList[3].innerText)
	let done = cellList[4].firstChild.checked

	// update-modal inputs
	let inputName = getElBySelector("#editToDo")
	let inputDueDate = getElBySelector("#editDueDate")
	let inputEstTime = getElBySelector("#editEstimated")
	let inputEstMin = getElBySelector("#editEstimatedMinutes")
	///////////////

	// show current values
	inputName.value = title
	inputDueDate.value = `${dueDate.getFullYear()}-${(dueDate.getMonth() + 1) > 9 ? dueDate.getMonth() + 1 : `0${dueDate.getMonth() + 1}`}-${dueDate.getDate()}`
	inputEstTime.value = estVals.std
	inputEstMin.value = estVals.min

	// update entry button
	let addEditedBtn = getElBySelector('#addEditedItem')
	// update entry event
	addEditedBtn.onclick = function () {
		let p = getElBySelector('#editTodoMessage')
		if (!isValidDeadline(new Date(inputDueDate.value), new Date())) {
			setPTag(p, "Invalide Deadline!", color)
			return
		}
		if (!isValidEstimated(inputEstTime.value, inputEstMin.value)) {
			setPTag(p, "Invalide Zeit-Schätzung!", color)
			return
		}
		// edit todo values of selected node
		cellList[0].innerText = inputName.value
		cellList[2].innerText = new Date(inputDueDate.value).toLocaleDateString()
		cellList[3].innerText = formatEstimatedTime(+inputEstTime.value, +inputEstMin.value)
		saveUpdate()
		updateModal.style.display = 'none'
	}
}
