import { setPTagMain, sendFile, createXHR, getElBySelector, drawCanvas, createNewJson } from "./dom_helper.js"

"use strict"

// Beinhaltet users und todos
let json
// Elemente selektieren
let container = getElBySelector("#main-content")
let pTag = getElBySelector("p")

// kennwort-test // is it needed as global var?
let sicherheitszahl = 1

init()

function init() {
	let myForm = getElBySelector("#registerform")
	let pwd = getElBySelector("#password")
	// passwort input listener
	pwd.addEventListener("keyup", function () {
		kennwortTest(this.value)
	})
	// form submit listener
	myForm.onsubmit = function () {
		let username = getElBySelector("#username")
		if (checkUsername(username)) {
			submitFunction(username, password)
		}
		// Absenden des Formulars unterbinden
		return false
	}
}

function kennwortTest(kennwort) {
	let xhr = createXHR()
	xhr.onload = function () {
		if (xhr.status !== 200) {
			console.log("file not found")
			createNewJson()
			return
		}
		// passwort sicherheit canvas c&p
		sicherheitszahl = +xhr.responseText > 50 ? 50 : +xhr.responseText
		drawCanvas(sicherheitszahl, 'cvs')
	}
	let encodeKeyWord = encodeURIComponent(kennwort)
	let url = "./php/pw_check.php?pw=" + encodeKeyWord
	xhr.open("GET", url)
	xhr.send()
}

function checkUsername(username) {
	if (username.value.indexOf(" ") > 0) {
		return false
	}
	let pattern = new RegExp('^[a-zA-Z][a-zA-Z0-9_-]*$')
	let isValid = (pattern.test(username.value))
	return isValid
}

function submitFunction(username, password) {
	let newUser = {
		username: username.value,
		password: password.value,
		todoList: []
	}
	let xhr = createXHR()
	xhr.onload = function () {
		if (xhr.status != 200) {
			console.log("file not found")
			createNewJson(newUser)
			return
		}
		json = xhr.response
		checkData(newUser)
	}
	xhr.open("GET", "./users.json")
	xhr.responseType = "json"
	xhr.setRequestHeader("Cache-Control", "no-cache")
	xhr.send()
}

function checkData(newUser) {
	// check if username is already available
	let newUsername = newUser.username
	let user
	for (let i = 0; i < json.users.length; i++) {
		if (json.users[i].username == newUsername) {
			user = json.users[i]
			break
		}
	}
	// existing user ....
	if (user) {
		let textContent = "\"" + newUsername + "\" existiert bereits!!!"
		let color = "red"
		setPTagMain(pTag, textContent, color, container)
		return
	}
	// user nicht vorhanden, müssen user anlegen
	json.users.push(newUser)
	sendFile(JSON.stringify(json), "./php/script.php")
	// user notification
	setPTagMain(pTag, newUsername + " erfolgreich registriert!", "blue", container)
	// redirect after timeout
	let timeout = setTimeout(function () {
		// write in localstorage as "logged-in"
		localStorage.setItem('user', newUsername)
		// redirect
		location.href = './todoList.html'
		clearTimeout(timeout)
	}, 1000)
}