"use strict"

// HILFSFUNKTIONEN
export function addModalListeners(modal, update = false) {
	if (!modal) { return }
	// When the user clicks anywhere outside of the modal, close it
	modal.onclick = function (event) {
		if (event.target == modal) {
			modal.style.display = "none"
		}
	}
}
// Fähigkeitenweiche für XHR
var createFunctions = [
	function () { return new XMLHttpRequest() },
	function () { return new ActiveXObject("Msxml2.XMLHTTP") },
	function () { return new ActiveXObject("Msxml3.XMLHTTP") },
	function () { return new ActiveXObject("Microsoft.XMLHTTP") }
]
export function createXHR() {
	var test, xmlHTTP = null
	for (var i = 0; i < createFunctions.length; i++) {
		try {
			test = createFunctions[i]
			xmlHTTP = test()
		} catch (e) {
			continue
		}
		break
	}
	return xmlHTTP
}

export function removeElement(e) {
	var elem = e.target
	var body = document.querySelector("body")
	if (elem != body && elem.querySelectorAll("body").length === 0) {
		var parent = elem.parentNode
		parent.removeChild(elem)
		return false
	}
}

export function createEle(ele, content) {
	//guard
	if (!ele) { return }
	const element = document.createElement(ele)
	if (content) {
		element.innerText = content
	}
	return element
}

export function formatEstimatedTime(hrs, min) {
	if (!isNumber(hrs)) { return hrs }
	if (hrs == 0) { return `${min}min.` }
	if (min == 0) { return `${hrs}std.` }
	return `${hrs}std : ${min}min` // hrs + 'std : ' + min + 'min'
}

export function formatMinutesToHrsMin(minuten) {
	let minut = minuten % 60
	let hrs = Math.floor(minuten / 60)
	return formatEstimatedTime(hrs, minut)
}

export function getEstimatedFromNode(node) {
	if (node.innerText.includes(':')) {
		let estimate = node.innerText.split(':')
		return parseInt(estimate[0]) * 60 + parseInt(estimate[1])
	}
	if (node.innerText.includes('std') && !node.innerText.includes('min')) {
		return parseInt(node.innerText) * 60
	}
	return parseInt(node.innerText)
}

export function getEstValues(valString) {
	if (valString.includes(':')) {
		let estimate = valString.split(':')
		return {
			std: parseInt(estimate[0]),
			min: parseInt(estimate[1])
		}
	}
	if (valString.includes('std') && !valString.includes('min')) {
		return {
			std: parseInt(valString),
			min: 0
		}
	}
	return {
		std: 0,
		min: parseInt(valString)
	}
}

export function getDuedFromNode(node) {
	let dateStr = node.innerText
	if (dateStr.indexOf(".")) {
		let dateArr = dateStr.split(".")
		dateStr = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0]
		//console.log("dateStr: ", dateStr);
	}

	let date = new Date(dateStr)
	//console.log("datum: ", date);

	return date.getTime()
}

export function updateButtonState() {
	let tbody = getElBySelector("tbody")
	if (tbody) {
		// TODO update json
		let allTableRows = tbody.querySelectorAll("tr")
		for (let i = 0; i < allTableRows.length; i++) {
			allTableRows[i].querySelector("button.nachOben").disabled = !allTableRows[i].previousElementSibling
			allTableRows[i].querySelector("button.nachUnten").disabled = !allTableRows[i].nextElementSibling
		}
	}
}

export function getElBySelector(selector) {
	if (!selector) { return null }
	return document.querySelector(selector)
}

export function getAllBySelector(selector) {
	if (!selector) { return null }
	return document.querySelectorAll(selector)
}

export function getUserFromJson(username, users) {
	// guard
	if (!username) { return }
	let userMatch
	// check user and pw
	for (let i = 0; i < users.length; i++) {
		const user = users[i]
		// good case, set userMatch var
		if (username === user.username) {
			userMatch = user
			break
		}
	}
	return userMatch
}

export function createNewJson(newUser) {
	let users = []
	if (newUser) {
		users.push(newUser)
	}
	let newJson = {
		users
	}
	sendFile(JSON.stringify(newJson), "./php/script.php")
}

export function sendFile(jsonString, url) {
	//console.log('jsonString: ', jsonString);
	console.log('url: ', url)
	let xhr = createXHR()
	xhr.onload = function () {
		if (xhr.status != 200) {
			console.log('status: ', xhr.status)
			return
		}
	}
	xhr.open("POST", url)
	xhr.send(jsonString)
}

export function formatDateString(dateString) {
	let splittedCreated = dateString.split('.')
	return new Date(+splittedCreated[2], +splittedCreated[1] - 1, +splittedCreated[0])
}

export function setPTag(ptag, message, color) {
	ptag.innerText = message
	ptag.style.color = color
	// container.append(ptag)
	let timeout = setTimeout(function () {
		ptag.innerText = ''
		clearTimeout(timeout)
	}, 2500)
}

export function appendTable(tableRow) {
	let thead = createEle('thead')
	let tbody = createEle('tbody')
	let headRow = createEle('tr')
	let headCell1 = createEle('th', 'To-Do')
	headCell1.className = 'firstTH'
	let headCell2 = createEle('th', 'Erstelldatum')
	let headCell3 = createEle('th', 'Deadline')
	let headCell4 = createEle('th', 'Zeit-Schätzung')
	let headCell5 = createEle('th', 'Erledigt')
	headRow.append(headCell1, headCell2, headCell3, headCell4, headCell5)
	thead.append(headRow)
	tbody.append(tableRow)
	getElBySelector('#liste').append(thead, tbody)
}

export function setPTagMain(ptag, message, color, container) {
	ptag.innerText = message
	ptag.style.color = color
	container.append(ptag)
	let timeout = setTimeout(function () {
		ptag.remove()
		clearTimeout(timeout)
	}, 2500)
}

export function checkExistingTodo(title) {
	let tbody = getElBySelector("tbody")
	if (!tbody) { return false }
	let titleList = tbody.querySelectorAll("td.todoId")
	for (let i = 0; i < titleList.length; i++) {
		let value = titleList[i].innerText
		if (title == value) {
			return true
		}
	}
	return false
}

export function isValidDeadline(dueDate, createdDate) {
	const oneDayinMs = 86400000
	const dueMs = dueDate.getTime()
	const createdMs = createdDate.getTime()
	return (createdMs - dueMs) < oneDayinMs
}

export function isValidEstimated(hrs, min) {
	return +hrs >= 0 && +min >= 0
}

export function logoutUser() {
	localStorage.removeItem('user')
	location.reload()
}

// CANVAS C&P
export function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
	if (typeof stroke == "undefined") {
		stroke = true
	}
	if (typeof radius === "undefined") {
		radius = 5
	}
	//edge clearing
	var gdest = ctx.globalCompositeOperation
	var fstyle = ctx.fillStyle
	ctx.globalCompositeOperation = "destination-out"
	ctx.fillStyle = "rgba(0,0,0,1.0)"
	ctx.beginPath()
	ctx.moveTo(x, y + radius)
	ctx.quadraticCurveTo(x, y, x + radius, y)
	ctx.lineTo(x, y)
	ctx.lineTo(x, y + radius)
	ctx.closePath()
	ctx.fill()
	ctx.beginPath()
	ctx.moveTo(x, y + height - radius)
	ctx.quadraticCurveTo(x, y + height, x + radius, y + height)
	ctx.lineTo(x, y + height)
	ctx.lineTo(x, y + height - radius)
	ctx.closePath()
	ctx.fill()
	ctx.beginPath()
	ctx.moveTo(x + width - radius, y)
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius)
	ctx.lineTo(x + width, y)
	ctx.lineTo(x + width - radius, y)
	ctx.closePath()
	ctx.fill()
	ctx.beginPath()
	ctx.moveTo(x + width, y + height - radius)
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
	ctx.lineTo(x + width, y + height)
	ctx.lineTo(x + width, y + height - radius)
	ctx.closePath()
	ctx.fill()
	ctx.fillStyle = fstyle
	ctx.globalCompositeOperation = gdest
	ctx.beginPath()
	ctx.moveTo(x + radius, y)
	ctx.lineTo(x + width - radius, y)
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius)
	ctx.lineTo(x + width, y + height - radius)
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
	ctx.lineTo(x + radius, y + height)
	ctx.quadraticCurveTo(x, y + height, x, y + height - radius)
	ctx.lineTo(x, y + radius)
	ctx.quadraticCurveTo(x, y, x + radius, y)
	ctx.closePath()
	if (stroke) {
		ctx.stroke()
	}
	if (fill) {
		ctx.fill()
	}
}
export function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n)
}
export function drawCanvas(percentage, canvasid) {
	percentage *= 2
	var error = !isNumber(percentage)
	if (!error && (percentage < 0 || percentage > 100)) {
		error = true
	}
	percentage = percentage.toString().replace(/[^0-9]/g, '')
	var canvas = getElBySelector('#' + canvasid)
	canvas.hidden = false
	canvas.clear = true
	canvas = document.getElementById('cvs')
	if (canvas.getContext) {
		var context = canvas.getContext('2d')
		context.clearRect(0, 0, canvas.width, canvas.height)
		context.fillStyle = "rgb(143, 143, 143)"
		context.fillRect(0, 0, canvas.width, canvas.height)
		var percWidth = Math.round(canvas.width / 100)
		if (!error) {
			for (var i = 0; i <= percentage; i++) {
				var r, g, b
				if (i <= 50) {
					r = 255
					g = Math.round((255 * i) / 50)
					b = 0
				}
				else {
					r = Math.round((255 * (100 - i)) / 50)
					g = 255
					b = 0
				}
				context.fillStyle = "rgb(" + r + ", " + g + ", " + b + ")"
				context.fillRect(Math.round(i * canvas.width / 100) - percWidth, 0, percWidth, canvas.height)
			}
			context.font = "bold 30px sans-serif"
			context.fillStyle = "rgb(255,255,255)"
			context.textBaseline = "middle"
			var msg = percentage + "%"
		}
		else {
			context.fillStyle = "rgb(255,0,0)"
			context.fillRect(0, 0, canvas.width, canvas.height)
			context.font = "bold 30px sans-serif"
			context.fillStyle = "rgb(255,255,255)"
			context.textBaseline = "middle"
			var msg = "ERROR: Wrong input"
		}
		context.fillStyle = "rgb(60,60,60)"
		context.fillText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2) + 2, canvas.height / 2 + 2)
		context.fillStyle = "rgb(90,90,90)"
		context.fillText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2) + 1, canvas.height / 2 + 1)
		context.fillStyle = "rgb(255,255,255)"
		context.fillText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2), canvas.height / 2)
		context.strokeText(msg, (canvas.width / 2) - (context.measureText(msg).width / 2), canvas.height / 2)
		context.globalAlpha = 0.4
		context.fillStyle = "rgb(255, 255, 255)"
		context.fillRect(0, 0, canvas.width, (canvas.height / 32) * 15)
		context.globalAlpha = 1.0
		context.fillStyle = "rgb(0, 0, 0)"
		roundRect(context, 0, 0, canvas.width, canvas.height, 10)
	}
}

// NOT USED ///////////////////////////////////////////////////////////////////
var isInViewport = function (elem) {
	var distance = elem.getBoundingClientRect()
	return (
		distance.top >= 0 &&
		distance.left >= 0 &&
		distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		distance.right <= (window.innerWidth || document.documentElement.clientWidth)
	)
}
// COOKIES
// Cookies setzen
function createCookie(name, value, days) {
	var expires = ''
	if (days) {
		var date = new Date()
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
		expires = '; expires=' + date.toUTCString()
	}
	document.cookie = name + '=' + value + expires + '; path=/'
}
// Cookies auslesen
function readCookie(name) {
	var nameEQ = name + '='
	var ca = document.cookie.split(';')
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i]
		while (c.charAt(0) == ' ') c = c.substring(1, c.length)
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
	}
	return null
}
// Cookies löschen
function deleteCookie(name) {
	createCookie(name, '', -1)
}
// Zufallszahlen erzeugen
function rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min
}
// Elemente dynamisch erzeugen
function createTags(parent, elem, content = "") {
	var container = document.querySelector(parent)
	var newElem = document.createElement(elem)
	newElem.innerHTML = content
	container.appendChild(newElem)
}
// Elemente dynamisch erzeugen - aus Object
function createTagsFromObject(elem) {
	if (typeof elem === "object" && Object.keys(elem).length != 0) {
		let container = document.querySelector(elem.parentTag)
		let newElem = document.createElement(elem.tagName)
		newElem.className = elem.classNames

		if (elem.attributes && typeof elem.attributes === "object") {
			for (let key in elem.attributes) {
				newElem.setAttribute(key, elem.attributes[key])
			}
		}
		if (elem.content) {
			newElem.innerHTML = elem.content
		}
		if (elem.event) {
			newElem.addEventListener(elem.event, elem.eventFunction)
		}

		if (elem.referenceElement) {
			let reference = container.querySelector(elem.referenceElement)
			// reference.before(newElem);
			container.insertBefore(newElem, reference)
		} else {
			container.appendChild(newElem)
		}

	}
}